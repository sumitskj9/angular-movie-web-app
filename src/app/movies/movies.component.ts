import { Component, OnInit } from '@angular/core';
import {MovieService} from '../movie.service';
import {Movie} from '../movie';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']

})
export class MoviesComponent implements OnInit {
movies: Movie[];
id: number;
name: string;
cover: string;

  constructor(private movieService: MovieService) { }

  ngOnInit() {
    this.movieService.getMovies()
      .subscribe(movies =>{
        this.movies = movies;
      });
  }

  addMovies(event){
    event.preventDefault();
    var newMovie={
      id: this.id,
      name: this.name,
      cover: this.cover
    }
    this.movieService.addMovies(newMovie)
      .subscribe(movie => {
        this.movies.push(movie);
        this.movieService.getMovies()
          .subscribe(movies =>{
            this.movies = movies;
          });
      });
  }
}
